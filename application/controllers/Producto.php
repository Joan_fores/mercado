<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Producto
 *
 * @author a048313441d
 */
class Producto extends CI_Controller {
    
    //creamos el controlador por defecto
    /*public function index(){
        $this->load->database();
        $this->load->model('market');
        $data['resultado'] = $this->market->get_producto();
        $this->load->view('lista',$data);
       
    }*/
   
    
    public function index(){
        $this->load->database();
        $this->load->helper('url');
        $this->load->model('market');
        $data['resultado'] = $this->market->get_producto();
        $this->load->view('lista',$data);
        /*echo '<pre>';
        print_r($resultado);
        echo '</pre>';*/
    }
    /*public function guardar(){
        $datos['titulo'] = 'Bien, el producto se ha guardado';
        $this->load->view('commons/header',$datos);
        $producto = [
            'nombre' => $this->input->post('nombre'),
            'precio' => $this->input->post('precio'),
            ];
        $this->market->guardar($producto);
        //$this->load->view('commons/footer');
    }*/
    public function nuevo_producto(){
        $this->load->helper(['form','url']);
        $this->load->library('form_validation');
        $this->load->model('market');
        $this->load->database();
        $this->form_validation->set_rules('id', 'id del producto', 'required');
        $this->form_validation->set_rules('nombreProd', 'nombre del producto', 'required');
        $this->form_validation->set_rules('precio', 'precio del producto', 'required');
        $this->form_validation->set_rules('existencias', 'existencias del producto', 'required');
        $this->form_validation->set_rules('descripcion', 'descripcion del producto', 'required');
        $this->form_validation->set_rules('nombreCat', 'nombre de la categoria', 'required');
        if ($this->form_validation->run() == FALSE) {
            $datos ['titulo'] = "Nuevo producto";
            $datos ['categorias'] = $this->market->get_categorias();
            $this->load->view('commons/header',$datos);
            $this->load->view('producto/ficha_nuevo_producto');
            //$this->load->view('commons/footer');
            }
            
        else {
            $datos['titulo'] = 'Bien, el producto se ha guardado';
            $this->load->view('commons/header',$datos);
            $producto = [
                        'id' => $this->input->post('id'),
                        'nombreProd' => $this->input->post('nombreProd'),
                        'precio' => $this->input->post('precio'),
                        'existencias' => $this->input->post('existencias'),
                        'descripcion' => $this->input->post('descripcion'),
                        ];
            $this->market->guardar($producto);
            //$this->load->view('commons/footer');
            }
    }
}