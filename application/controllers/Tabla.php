<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Producto
 *
 * @author a048313441d
 */
class Tabla extends CI_Controller {
    
    //creamos el controlador por defecto
    /*public function index(){
        $this->load->database();
        $this->load->model('market');
        $data['resultado'] = $this->market->get_producto();
        $this->load->view('lista',$data);
       
    }*/
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->model('market');
    }    
        
    public function tabla(){
        $this->load->database();
        $this->load->helper('url');
        $this->load->model('market');
        $data['resultado'] = $this->market->get_producto();
        $this->load->view('productos/tabla',$data);
        /*echo '<pre>';
        print_r($resultado);
        echo '</pre>';*/
    }
    //Este es el nuevo controlador
    public function tabla1(){
        $data['resultado'] = $this->market->get_producto();
        $this->load->view('productos/tabla',$data);
    }    
}