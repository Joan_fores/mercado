<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
    <head>
        <meta charset="UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <title>Mi primera Vista</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <h1 class="text-white bg-danger col-12">
                    Lista de Artículos
                </h1>
            </div>    
            <div class="row">  
                <?php foreach ($resultado as $producto): ?>
                <div class="col-sm-4">
                    <div class="card">
                        <img src="<?php echo base_url('assets/images/productos/'.$producto->id.'.jpg');?>" class="card-img-top" alt="<?php echo $producto->nombreProd; ?>">
                        <strong  class="text-center text-uppercase card-title bg-danger text-white"><?php echo $producto->nombreProd; ?></strong>
                        <p class="card-body">
                            <?php echo substr($producto->descripcion,0,100)." ..."; ?>
                             
                            <span style="color:blue"> ( <?php echo $producto->existencias; ?> unidades )</span>
                        </p>
                        <p class="text-right ">
                            <strong class="alert alert-info">
                                <?php echo $producto->precio.' €'; ?>
                            </strong>
                        </p>    
                    </div>
                </div>
                <?php endforeach;?>
            </div>    
        </div>    
    </body>
</html>

