<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<html>
    <head>
        <meta charset="UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <title>Mi primera Vista</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <h1 class="text-white bg-danger col-12">
                    Añadir nuevo producto
                </h1>
            </div>
            
<?php echo form_open(site_url('producto/nuevo_producto'), ['class'=>'form-horizontal']);?>
            <div class="form-row">
                <div class="col-1">
                    <?php echo form_label('ID:', 'id');?>
                    <?php $marca = form_error('id') !== ''? 'border-danger bg-warning':'';?>
                    <?php echo form_input(['name'=> 'id', 'id'=>'id', 'class'=>"form-control $marca", 'value' => set_value('id')]);?>
                    <?php echo form_error('id', '<div class="small text danger">','</div>');?>
                </div>
                
                <div class="col-4">
                    <?php echo form_label('NOMBRE', 'nombreProd');?>
                    <?php $marca = form_error('nombreProd') !== ''? 'border-danger bg-warning':'';?>
                    <?php echo form_input(['name'=> 'nombreProd', 'id'=>'nombreProd', 'class'=>"form-control $marca", 'value' => set_value('nombreProd')]);?>
                    <?php echo form_error('nombreProd', '<div class="small text danger">','</div>');?>
                </div>   
            </div>
            <div class="form-row">
                <div class="col-1">
                    <?php echo form_label('PRECIO', 'precio');?>
                    <?php $marca = form_error('precio') !== ''? 'border-danger bg-warning':'';?>
                    <?php echo form_input(['name'=> 'precio', 'id'=>'precio', 'class'=>"form-control $marca", 'value' => set_value('precio')]);?>
                    <?php echo form_error('precio', '<div class="small text danger">','</div>');?>
                </div>
                
                <div class="col-2">
                    <?php echo form_label('EXISTENCIAS', 'existencias');?>
                    <?php $marca = form_error('existencias') !== ''? 'border-danger bg-warning':'';?>
                    <?php echo form_input(['name'=> 'existencias', 'id'=>'existencias', 'class'=>"form-control $marca", 'value' => set_value('existencias')]);?>
                    <?php echo form_error('precio', '<div class="small text danger">','</div>');?>
                </div>
                <div class="col-2">
                    <?php echo form_label('CATEGORIA', 'nombreCat');?>
                    <?php $marca = form_error('nombreCat') !== ''? 'border-danger bg-warning':'';?>
                    <?php echo form_dropdown('categoria', $categorias, 1, ['id'=>'categoria','class'=>'form-control']);?>
                    <?php echo form_error('nombreCat', '<div class="small text danger">','</div>');?>
                </div>
            </div>
            <div class="form-row">
                <div class="col-5">
                    <?php echo form_label('DESCRIPCIÓN', 'descripcion');?>
                    <?php $marca = form_error('descripcion') !== ''? 'border-danger bg-warning':'';?>
                    <?php echo form_input(['name'=> 'descripcion', 'id'=>'descripcion', 'class'=>"form-control $marca", 'value' => set_value('descripcion')]);?>
                    <?php echo form_error('precio', '<div class="small text danger">','</div>');?>
                </div>
            </div>
            <div class="form-row">
                <div class="col-1">
                    
                    <?php echo form_submit('enviar', 'Guardar');?>
                
                </div>
            </div>    
             <?php echo form_close();?>   
</div>
    
            
